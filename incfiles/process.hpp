#ifndef PROCESS_HPP
#define PROCESS_HPP

#include "core.hpp"

namespace SB {

class process
{

public:

    // Properties

    bool move;
    bool success;
    int complexity;

    size_t userMovesDone = 0;
    size_t pcMovesDone = 0;

    std::vector<size_t> cellsToHit;

    QTimer *moveFinTimer;
    QTimer *moveResTimer;

    // Constructor

    process(int complexity);

};

}

#endif // PROCESS_HPP

#include "prefield.hpp"

namespace SB {

/*
==================================
        CONSTRUCTORS
==================================
*/

PreField::PreField(QWidget *parent, QMap<QString, QString> *phrases, QString langpath)
    : field::field(parent, phrases, langpath)
{
    background = core::widget(parent, "preField", QRect(340, 20, 320, 320));
    abstraction = new AbstractField(false);

    setCells();
    setFieldLegend();
    setShipList(QRect(680, 20, 120, 120), true);
    setButtonsSection();
    setDescription();
    setStatistics();
    setConnections();

}

/*
==================================
             SETTERS
==================================
*/

void PreField::setButtonsSection()
{
    buttonsSection = core::widget(static_cast<QWidget*>(this->parent()), "buttonsSection", QRect(340, 332, 320, 78));
    acceptButton = core::button(buttonsSection, "acceptButton", (*phrases)["accept"], QRect(2, 20, 96, 28), false);
    atRandomButton = core::button(buttonsSection, "atRandomButton", (*phrases)["random"], QRect(104, 20, 112, 28));
    clearButton = core::button(buttonsSection, "clearButton", (*phrases)["clear"], QRect(222, 20, 96, 28));
}

void PreField::setConnections()
{
    for (size_t i = 0; i < 10; ++i) {
        for (size_t j = 0; j <10; ++j) {
            connect(cells[i][j], &QPushButton::clicked, this, &PreField::buildShip);
        }
    }

    for (size_t i = 0; i < 4; ++i) {
        connect(ships[i]->itself, &QPushButton::clicked, this, &PreField::allowPlacement);
    }

    connect(atRandomButton, &QPushButton::clicked, this, &PreField::generateAtRandom);
    connect(clearButton, &QPushButton::clicked, this, &PreField::clearAll);

}

void PreField::setDescription()
{
    description = core::label(static_cast<QWidget*>(this->parent()), "description", core::getFromFile(QString("languages/%1/texts/start.txt").arg(langpath)), QRect(20, 20, 300, 400));
    description->setAlignment(Qt::AlignLeft | Qt::AlignTop);
}

void PreField::setStatistics()
{
    statistics = core::label(static_cast<QWidget*>(this->parent()), "statistics", QString("%1:\n0/10").arg((*phrases)["shipsplaced"]), QRect(830, 20, 160, 400));
    statistics->setAlignment(Qt::AlignHCenter | Qt::AlignTop);
}

/*
==================================
             SLOTS
==================================
*/

void PreField::allowPlacement()
{
   currentShip = (static_cast<QPushButton*>(sender())->property("parentObject")).value<ship*>();
}

void PreField::buildShip()
{
    char position;
    QPushButton* cell = static_cast<QPushButton*>(sender());
    if (!currentShip) return;
    if (!cell->property("active").value<bool>()) {

        cell->setProperty("active", true);
        cell->setStyleSheet("background: #42af4a;");

        if (isNear(cell, position)) {
            ++definedCells;
        } else {

            for (auto i : previousParts) {
                i->setStyleSheet("");
                i->setProperty("active", false);
            }
            previousParts.clear();
            definedCells = 1;
        }

        previousParts.push_back(cell);

        if (definedCells == currentShip->type) {
            if (currentShip->type == 1) {
                size_t string = cell->property("string").value<size_t>();
                size_t column = cell->property("column").value<size_t>();
                this->abstraction->addShip(AbstractShip({column, string}));
            } else {

                size_t maxCol, minCol, maxStr, minStr;

                if (position == 'H') {

                    maxStr = minStr = cell->property("string").value<size_t>();
                    minCol = previousParts.front()->property("column").value<size_t>();
                    maxCol = previousParts.back()->property("column").value<size_t>();

                    for (auto b : previousParts) {
                        if (b->property("column").value<size_t>() > maxCol)
                            maxCol = b->property("column").value<size_t>();
                        if (b->property("column").value<size_t>() < minCol)
                            minCol = b->property("column").value<size_t>();
                    }
                }

                if (position == 'V') {

                    maxCol = minCol = cell->property("column").value<size_t>();
                    minStr = previousParts.front()->property("string").value<size_t>();
                    maxStr = previousParts.back()->property("string").value<size_t>();

                    for (auto b : previousParts) {
                        if (b->property("string").value<size_t>() > maxStr)
                            maxStr = b->property("string").value<size_t>();
                        if (b->property("string").value<size_t>() < minStr)
                            minStr = b->property("string").value<size_t>();
                    }
                }

                this->abstraction->addShip(AbstractShip({minCol, minStr}, {maxCol, maxStr}));
            }

            for (auto b : previousParts) {
                b->setProperty("active", false);
                b->setStyleSheet("");
            }
            previousParts.clear();

            bool needChange = false;

            switch (currentShip->type) {
            case 4:
                currentShip->itself->setText(QString("%1").arg(--fourShpis));
                if (!fourShpis) {
                    currentShip->itself->setEnabled(false);
                    needChange = true;
                }
                break;
            case 3:
                currentShip->itself->setText(QString("%1").arg(--trippleShips));
                if (!trippleShips) {
                    currentShip->itself->setEnabled(false);
                    needChange = true;
                }
                break;
            case 2:
                currentShip->itself->setText(QString("%1").arg(--doubleShips));
                if (!doubleShips) {
                    currentShip->itself->setEnabled(false);
                    needChange = true;
                }
                break;
            case 1:
                currentShip->itself->setText(QString("%1").arg(--singleShips));
                if (!singleShips) {
                    currentShip->itself->setEnabled(false);
                    needChange = true;
                }
                break;
            }

            definedCells = 0;
            ++aliveShips;
            changeStatistics();
            reflectAbstraction(core::ships);

            if (aliveShips == 10) {
                acceptButton->setEnabled(true);
                for (size_t i = 0; i < 10; ++i) {
                    for (size_t j = 0; j < 10; ++j) {
                        cells[i][j]->setEnabled(false);
                    }
                }
                return;
            }

            if (needChange) {
                if (fourShpis) currentShip = ships[0];
                else if (trippleShips) currentShip = ships[1];
                else if (doubleShips) currentShip = ships[2];
                else currentShip = ships[3];
            }

            currentShip->itself->setChecked(true);

        }

    } else {

        cell->setStyleSheet("");
        cell->setProperty("active", false);
        auto res = std::find(previousParts.begin(), previousParts.end(), cell);
        if (res != previousParts.end()) previousParts.erase(res);
        --definedCells;
    }

}

void PreField::changeStatistics()
{
    statistics->setText(QString("%1:\n%2/10").arg((*phrases)["shipsplaced"]).arg(aliveShips));
}

void PreField::clearAll()
{
    clearCells();
    definedCells = 0;
    aliveShips = 0;
    fourShpis = 1;
    trippleShips = 2;
    doubleShips = 3;
    singleShips = 4;
    previousParts.clear();
    for (auto s : ships) {
        s->itself->setEnabled(true);
        s->itself->setText(QString("%1").arg(5 - s->type));
    }
    currentShip = ships[0];
    currentShip->itself->setChecked(true);
    acceptButton->setEnabled(false);
    changeStatistics();
    abstraction->clear();
}

void PreField::clearCells(bool enabled)
{
    for (size_t i = 0; i < 10; ++i) {
        for (size_t j = 0; j < 10; ++j) {
            cells[i][j]->setStyleSheet("");
            cells[i][j]->setEnabled(enabled);
            cells[i][j]->setProperty("active", false);
        }
    }
}

void PreField::generateAtRandom()
{
    clearCells();
    abstraction = new AbstractField(true);
    abstraction->optimize();
    reflectAbstraction(core::all);
    acceptButton->setEnabled(true);
    aliveShips = 10;
    fourShpis = 0;
    trippleShips = 0;
    doubleShips = 0;
    singleShips = 0;
    previousParts.clear();
    for (auto s : ships) {
        s->itself->setEnabled(false);
        s->itself->setText(QString("0"));
    }

    changeStatistics();
}

/*
==================================
             OTHERS
==================================
*/

bool PreField::isNear(QPushButton *b, char &pos)
{
    if (!previousParts.size()) return true;
    bool result = false;
    int direction;
    int string = b->property("string").value<int>();
    int column = b->property("column").value<int>();
    QPushButton *to_compare;

    for (auto i : previousParts) {

        int pp_string = i->property("string").value<int>();
        int pp_column = i->property("column").value<int>();

        if ((abs(pp_string - string) == 1 && !(pp_column - column)) ||
           (abs(pp_column - column) == 1 && !(pp_string - string))) {
            result = true;
            to_compare = i;
            direction = pp_string - string ? core::vertical : core::horizontal;
            pos = (direction == core::vertical ? 'V' : 'H');
            break;
       }
    }

    if (!result) return false;

    string = to_compare->property("string").value<int>();
    column = to_compare->property("column").value<int>();

    for (auto i = previousParts.begin(); i < previousParts.end(); ++i) {
        QPushButton* cb = *i;
        if (cb == to_compare) continue;
        int pp_string = cb->property("string").value<int>();
        int pp_column = cb->property("column").value<int>();
        if (((pp_string - string) && (direction == core::horizontal)) ||
            ((pp_column - column) && (direction == core::vertical))) {
                previousParts.erase(i);
                cb->setStyleSheet("");
                cb->setProperty("active", false);
                --i;
                --definedCells;
            }
    }
    return result;

}

}

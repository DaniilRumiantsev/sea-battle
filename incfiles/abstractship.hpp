#ifndef ABSTRACTSHIP_HPP
#define ABSTRACTSHIP_HPP

#include "core.hpp"

namespace SB {

class AbstractShip {

public:

    char position;
    size_t crashedParts = 0;
    std::vector<coo> location;
    std::vector<size_t> environment;

    AbstractShip(coo placement);
    AbstractShip(coo front, coo back);

};

}

#endif // ABSTRACTSHIP_HPP

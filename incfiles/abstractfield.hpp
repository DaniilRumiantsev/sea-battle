#ifndef ABSTRACTFIELD_HPP
#define ABSTRACTFIELD_HPP

#include "core.hpp"
#include "abstractship.hpp"

namespace SB {

class AbstractField
{

public:

    char square[10][10];
    char direction;

    std::size_t column;
    std::size_t string;
    std::size_t extraSpace;
    std::vector<size_t> availableCells;
    std::vector<AbstractShip> ships;

    // Constructor

     AbstractField(bool atRand = false);

    // Common functions

    void allowAllCells();
    void clear();
    void crashCell(size_t i, size_t j);
    void fill();
    void optimize();
    void redefine(AbstractShip &s, bool reduce = false);

    // Functions for usual initialization

    void addShip(AbstractShip s);

    // Functions for random initialization

    bool continueDown();
    bool continueLeft();
    bool continueRight();
    bool continueUp();
    bool defineDirection(const char potentialDirection);
    void initializeAtRandom();
    void reduceAvailableCells(AbstractShip& s);

};

}

#endif // ABSTRACTFIELD_HPP

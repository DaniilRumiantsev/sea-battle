#ifndef FIELD_HPP
#define FIELD_HPP

#include "core.hpp"
#include "ship.hpp"
#include "abstractfield.hpp"

namespace SB {

class field : public QWidget
{
    Q_OBJECT

public:

    // Properties

    size_t aliveShips = 0;
    size_t definedCells = 0;
    size_t fourShpis = 1;
    size_t trippleShips = 2;
    size_t doubleShips = 3;
    size_t singleShips = 4;

    AbstractField *abstraction;
    ship *ships[4];
    ship *currentShip;
    std::vector<QPushButton*> previousParts;

    QWidget *background;
    QWidget *buttonsSection;
    QWidget *shipList;
    QPushButton *acceptButton;
    QPushButton *atRandomButton;
    QPushButton *clearButton;
    QPushButton *cells[10][10];
    QLabel *labels[20];
    QMap<QString, QString> *phrases;
    QString langpath;

    // Constructor

    explicit field(QWidget *parent = 0, QMap<QString, QString>* phrases = 0, QString langpath = "en");

    // Setters

    void setCells();
    void setFieldLegend();
    void setShipList(QRect listGeometry, bool pre = false);

    // Other

    void reflectAbstraction(int to_disable = 0);

};

}

Q_DECLARE_METATYPE(SB::field*)

#endif // FIELD_HPP

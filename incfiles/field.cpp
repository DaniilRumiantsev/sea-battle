#include "field.hpp"

namespace SB {

/*
==================================
          CONSTRUCTORS
==================================
*/

field::field(QWidget *parent, QMap<QString, QString> *phrases, QString langpath)
    : QWidget(parent)
{
    this->phrases = phrases;
    this->langpath = langpath;
}

/*
==================================
             SETTERS
==================================
*/

void field::setCells()
{
    QVariant parentObject = QVariant();
    parentObject.setValue(this);

    for (size_t i = 0; i < 10; ++i) {
        for (size_t j = 0; j <10; ++j) {
            cells[i][j] = core::button(background, QString("b%1").arg(core::fromCooToStraight(i, j)), "", QRect(30*j + 21, 30*i + 21, 28, 28));
            cells[i][j]->setProperty("active", false);
            cells[i][j]->setProperty("string", i + 1);
            cells[i][j]->setProperty("column", j + 1);
            cells[i][j]->setProperty("field", parentObject);
        }
    }
}

void field::setFieldLegend()
{
    for (int i = 0; i < 10; ++i) {
        labels[i] = core::label(background, QString("vl%1").arg(i + 1), QString("%1").arg(i + 1), QRect(1, 30*i + 21, 18, 28));
        labels[i]->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    }

    for (int i = 0; i < 10; ++i) {
        labels[i] = core::label(background, QString("hl%1").arg(i + 1), QString("%1").arg(char('A' + i)), QRect(30*i + 21, 1, 28, 18));
        labels[i]->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    }
}

void field::setShipList(QRect listGeometry, bool pre)
{
    shipList = core::widget(static_cast<QWidget*>(this->parent()), "shipList", listGeometry);
    for (size_t i = 0; i < 4; ++i) ships[i] = new ship(shipList, 4 - i, pre);
    currentShip = ships[0];
}

/*
==================================
             OTHERS
==================================
*/

void field::reflectAbstraction(int to_disable)
{
    for (size_t i = 0; i < 10; ++i) {
        for (size_t j = 0; j < 10; ++j) {
            if (to_disable == core::all) cells[i][j]->setDisabled(true);
            if (abstraction->square[i][j] == 'A') {
                if (to_disable == core::ships) cells[i][j]->setDisabled(true);
                cells[i][j]->setStyleSheet("background: url(style/images/graphics/shippart.png); border-radius: 0;");
            }
            if ((to_disable == core::ships) && (abstraction->square[i][j] == 'U')) {
                cells[i][j]->setDisabled(true);
            }
        }
    }
}

}

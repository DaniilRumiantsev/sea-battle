#include "realfield.hpp"

/*
==================================
        CONSTRUCTORS
==================================
*/

namespace SB {

RealField::RealField(QWidget *parent, QMap<QString, QString> *phrases, QString langpath, QString name, QRect geomerty, AbstractField *abstractField, QRect shipListGeometry)
    : field::field(parent, phrases, langpath)
{
    aliveShips = 10;
    background = core::widget(parent, name, geomerty);
    abstraction = abstractField;

    setCells();
    setFieldLegend();
    setShipList(shipListGeometry);
}

/*
==================================
            SLOTS
==================================
*/

bool RealField::eraseCell(size_t i, size_t j)
{
    abstraction->crashCell(i, j);

    if (abstraction->square[i][j] == 'A') {
        abstraction->square[i][j] = 'D';
        cells[i][j]->setEnabled(false);
        cells[i][j]->setStyleSheet("background: url(style/images/graphics/crashedship.png);");
        return true;
    } else {
        abstraction->square[i][j] = 'C';
        cells[i][j]->setEnabled(false);
        cells[i][j]->setStyleSheet("background: url(style/images/graphics/crashed.png);");
        return false;
    }
}

/*
==================================
           OTHERS
==================================
*/

void RealField::alterField(coo crashed, std::vector<size_t> *cellsToHit, int complexity)
{
    for (auto& s : abstraction->ships) {
        for (size_t x = s.location.front().x, endx = s.location.back().x; x <= endx; ++x) {
            for (size_t y = s.location.front().y, endy = s.location.back().y; y <= endy; ++y) {
                if (x == crashed.x && y == crashed.y) {
                    ++s.crashedParts;
                    size_t length = (s.position == 'H' ? s.location.back().x - s.location.front().x : s.location.back().y - s.location.front().y) + 1;

                    if (s.crashedParts == length) {

                        for (auto&& straight : s.environment) {
                            size_t col, str;
                            core::fromStraightToCoo(straight, col, str);
                            if (abstraction->square[str - 1][col - 1] == 'F') abstraction->square[str - 1][col - 1] = 'C';
                            if (abstraction->square[str - 1][col - 1] == 'A') abstraction->square[str - 1][col - 1] = 'D';
                            QString style = QString("background: url(style/images/graphics/crashed%1.png);").arg(abstraction->square[str - 1][col - 1] == 'D' ? "ship" : "");
                            cells[str - 1][col - 1]->setEnabled(false);
                            cells[str - 1][col - 1]->setStyleSheet(style);
                            auto res = std::find(abstraction->availableCells.begin(), abstraction->availableCells.end(), straight);
                            if (res != abstraction->availableCells.end()) abstraction->availableCells.erase(res);
                        }
                        --aliveShips;
                        abstraction->ships.erase(static_cast<std::vector<AbstractShip>::iterator>(&s));

                        switch (length) {
                        case 4: ships[0]->itself->setText(QString("%1").arg(--fourShpis)); break;
                        case 3: ships[1]->itself->setText(QString("%1").arg(--trippleShips)); break;
                        case 2: ships[2]->itself->setText(QString("%1").arg(--doubleShips)); break;
                        case 1: ships[3]->itself->setText(QString("%1").arg(--singleShips)); break;
                        }

                        if (!fourShpis) ships[0]->itself->setStyleSheet("background: #1c1c1c url(style/images/graphics/fourshipcrashed.png) no-repeat;");
                        if (!trippleShips) ships[1]->itself->setStyleSheet("background: #1c1c1c url(style/images/graphics/trippleshipcrashed.png) no-repeat;");
                        if (!doubleShips) ships[2]->itself->setStyleSheet("background: #1c1c1c url(style/images/graphics/doubleshipcrashed.png) no-repeat;");
                        if (!singleShips) ships[3]->itself->setStyleSheet("background: #1c1c1c url(style/images/graphics/singleshipcrashed.png) no-repeat;");

                    } else if (cellsToHit != nullptr) {

                        size_t x  = crashed.x, y = crashed.y;

                        if (((s.crashedParts == 1) && (complexity == 2)) || (complexity == 1)) {

                            if ((x - 1 >= 1) && ((abstraction->square[y-1][x-2] == 'F') || (abstraction->square[y-1][x-2] == 'A')))
                                cellsToHit->push_back(core::fromCooToStraight(y - 1, x - 2));
                            if ((x + 1 <= 10) && ((abstraction->square[y-1][x] == 'F') || (abstraction->square[y-1][x] == 'A')))
                                cellsToHit->push_back(core::fromCooToStraight(y - 1, x));
                            if ((y - 1 >= 1) && ((abstraction->square[y-2][x-1] == 'F') || (abstraction->square[y-2][x-1] == 'A')))
                                cellsToHit->push_back(core::fromCooToStraight(y - 2, x - 1));
                            if ((y + 1 <= 10) && ((abstraction->square[y][x-1] == 'F') || (abstraction->square[y][x-1] == 'A')))
                                cellsToHit->push_back(core::fromCooToStraight(y, x - 1));

                        }

                        if ((complexity == 3) || ((complexity == 2) && (s.crashedParts > 1))) {

                            if (s.position == 'H') {
                                size_t nextX = x, prevX = x;
                                while ((++nextX <= 10) && (abstraction->square[y-1][nextX-1] == 'D'));
                                while ((--prevX >=  1) && (abstraction->square[y-1][prevX-1] == 'D'));
                                if ((nextX <= 10) && ((abstraction->square[y-1][nextX-1] == 'F') || (abstraction->square[y-1][nextX-1] == 'A')))
                                    cellsToHit->push_back(core::fromCooToStraight(y - 1, nextX - 1));
                                if ((prevX >= 1) && ((abstraction->square[y-1][prevX-1] == 'F') || (abstraction->square[y-1][prevX-1] == 'A')))
                                    cellsToHit->push_back(core::fromCooToStraight(y - 1, prevX - 1));
                            }

                            if (s.position == 'V') {
                                size_t nextY = y, prevY = y;
                                while ((++nextY <= 10) && (abstraction->square[nextY-1][x-1] == 'D'));
                                while ((--prevY >=  1) && (abstraction->square[prevY-1][x-1] == 'D'));
                                if ((nextY <= 10) && ((abstraction->square[nextY-1][x-1] == 'F') || (abstraction->square[nextY-1][x-1] == 'A')))
                                    cellsToHit->push_back(core::fromCooToStraight(nextY - 1, x - 1));
                                if ((prevY >= 1) && ((abstraction->square[prevY-1][x-1] == 'F') || (abstraction->square[prevY-1][x-1] == 'A')))
                                    cellsToHit->push_back(core::fromCooToStraight(prevY - 1, x - 1));
                            }

                        }
                    }

                    return;
                }
            }
        }
    }
}

}

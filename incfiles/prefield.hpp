#ifndef PREFIELD_HPP
#define PREFIELD_HPP

#include "core.hpp"
#include "field.hpp"

namespace SB {

class PreField : public field
{

public:

    // Properties

    QLabel *statistics;
    QLabel *description;

    // Constructor

    PreField(QWidget *parent = 0, QMap<QString, QString> *phrases = 0, QString langpath = "en");

    // Setters

    void setButtonsSection();
    void setConnections();
    void setDescription();
    void setStatistics();

    // Slots

    void allowPlacement();
    void buildShip();
    void changeStatistics();
    void clearAll();
    void clearCells(bool enabled = true);
    void generateAtRandom();

    // Other

    bool isNear(QPushButton *b, char& pos);

};

}

#endif // PREFIELD_HPP

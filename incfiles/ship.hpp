#ifndef SHIP_HPP
#define SHIP_HPP

#include "core.hpp"

namespace SB {

class ship
{

public:

    // Prorerties

    size_t type;

    QPushButton *itself;

    // Constructor

    explicit ship(QWidget *parent, size_t type, bool pre = false);

};

}

Q_DECLARE_METATYPE(SB::ship*)


#endif // SHIP_HPP

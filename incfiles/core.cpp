#include "core.hpp"

namespace SB {

QString core::getFromFile(QString filename)
{
    QString res = "";
    QFile f(filename);
    if (f.open(QFile::ReadOnly)) res = f.readAll();
    return res;
}

QWidget* core::widget(QWidget *parent, QString name, QRect geometry, bool show)
{
    QWidget *w = new QWidget(parent);
    w->setObjectName(name);
    if (geometry != QRect(0,0,0,0)) w->setGeometry(geometry);
    if (show) w->show(); else w->hide();
    return w;
}

QLabel* core::label(QWidget *parent, QString name, QString text, QRect geometry)
{
    QLabel *l = new QLabel(parent);
    l->setObjectName(name);
    if (geometry != QRect(0,0,0,0)) l->setGeometry(geometry);
    if (text != QString("")) l->setText(text);
    l->setWordWrap(true);
    l->show();
    return l;
}

QPushButton* core::button(QWidget *parent, QString name, QString text, QRect geometry, bool enabled)
{
    QPushButton *b = new QPushButton(parent);
    b->setObjectName(name);
    if (geometry != QRect(0,0,0,0)) b->setGeometry(geometry);
    if (text != QString("")) b->setText(text);
    b->setEnabled(enabled);
    b->show();
    return b;
}

QCheckBox* core::checkBox(QWidget *parent, QString name, QString text, QRect geometry, bool enabled, bool checked)
{
    QCheckBox *b = new QCheckBox(parent);
    b->setObjectName(name);
    b->setGeometry(geometry);
    if (text != QString("")) b->setText(text);
    b->setEnabled(enabled);
    b->setChecked(checked);
    b->show();
    return b;
}

QRadioButton* core::radioButton(QWidget *parent, QString name, QString text, QRect geometry, int value, bool enabled, bool checked)
{
    QRadioButton *b = new QRadioButton(parent);
    b->setObjectName(name);
    b->setGeometry(geometry);
    if (text != QString("")) b->setText(text);
    b->setProperty("value", value);
    b->setEnabled(enabled);
    b->setChecked(checked);
    b->show();
    return b;
}

QSlider* core::slider(QWidget *parent, QString name, QRect geometry, int min, int max, int value)
{
    QSlider *s = new QSlider(parent);
    s->setObjectName(name);
    s->setGeometry(geometry);
    s->setOrientation(Qt::Horizontal);
    s->setMinimum(min);
    s->setMaximum(max);
    s->setValue(value);
    s->show();
    return s;
}

QVBoxLayout* core::verticalLayout(QWidget* parent, QString name, QMargins margins, size_t spacing)
{
    QVBoxLayout* v = new QVBoxLayout(parent);
    v->setObjectName(name);
    v->setContentsMargins(margins);
    v->setSpacing(spacing);
    return v;
}

size_t core::number(size_t min, size_t max)
{
    std::random_device rn1;
    int rn2 = rand();
    if (!rn2) rn2 = 1;
    return (rn1()%rn2)%(max-min+1) + min;
}

size_t core::fromCooToStraight(size_t i, size_t j)
{
    return i*10 + j + 1;
}

void core::fromStraightToCoo(size_t straight, size_t &j, size_t &i)
{
    j = (straight%10 == 0 ? 10 : straight%10);
    i = straight/10 + (j == 10 ? 0 : 1);
}

char core::direction()
{
    return "LRUD"[number(0, 3)];
}

}

#ifndef REALFIELD_HPP
#define REALFIELD_HPP

#include "core.hpp"
#include "field.hpp"

namespace SB {

class RealField : public field
{

public:

    // Constructor

    RealField(QWidget *parent, QMap<QString, QString>* phrases = 0, QString langpath = "en", QString name = "", QRect geomerty = QRect(0,0,0,0), AbstractField *abstractField = 0, QRect shipListGeometry = QRect(0,0,0,0));

    // Slots

    bool eraseCell(size_t i, size_t j);

    // Other

    void alterField(coo crashed, std::vector<size_t> *cellsToHit = nullptr, int complexity = 2);

};

}

#endif // REALFIELD_HPP

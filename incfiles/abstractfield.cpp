#include "abstractfield.hpp"

namespace SB {

/*
=============================================
                CONSTRUCTORS
=============================================
*/

AbstractField::AbstractField(bool atRand)
{
    allowAllCells();
    fill();
    if (atRand) initializeAtRandom();
}

/*
=============================================
            COMMON FUNCTIONS
=============================================
*/

void AbstractField::allowAllCells()
{
    availableCells.clear();
    for (size_t n = 1; n <= 100; ++n) availableCells.push_back(n);
}

void AbstractField::clear()
{
    string = column = extraSpace = 0;
    availableCells.clear();
    ships.clear();
    fill();
}

void AbstractField::crashCell(size_t i, size_t j)
{
    size_t cell = core::fromCooToStraight(i, j);
    auto res = std::find(availableCells.begin(), availableCells.end(), cell);
    availableCells.erase(res);
}

void AbstractField::fill()
{
    for (size_t i = 0; i < 10; ++i) {
        for (size_t j = 0; j < 10; ++j) {
            square[i][j] = 'F';
        }
    }
}

void AbstractField::optimize()
{
    for (size_t i = 0; i < 10; ++i) {
        for (size_t j = 0; j < 10; ++j)
        if (square[i][j] == 'U') square[i][j] = 'F';
    }
}

void AbstractField::redefine(AbstractShip& s, bool reduce)
{
    size_t fx = s.location.front().x;
    size_t fy = s.location.front().y;
    size_t bx = s.location.back().x;
    size_t by = s.location.back().y;

    bool leftSide = (fx > 1 ? true : false);
    bool rightSide = (bx < 10 ? true : false);
    bool topSide = (fy > 1 ? true : false);
    bool bottomSide = (by < 10 ? true : false);

    switch (s.position) {

    case 'V':

        if (topSide) {
            square[fy - 2][fx - 1] = 'U';
            s.environment.push_back((fy - 2)*10 + fx);
            if (leftSide) {
                square[fy - 2][fx - 2] = 'U';
                s.environment.push_back((fy - 2)*10 + fx - 1);
            }
            if (rightSide) {
                square[fy - 2][fx] = 'U';
                s.environment.push_back((fy - 2)*10 + fx + 1);
            }
        }

        for (size_t i = fy; i <= by; ++i) {
            if (leftSide) {
                square[i - 1][fx - 2] = 'U';
                s.environment.push_back((i - 1)*10 + fx - 1);
            }
            if (rightSide) {
                square[i - 1][fx] = 'U';
                s.environment.push_back((i - 1)*10 + fx + 1);
            }
        }

        if (bottomSide) {
            square[by][bx - 1] = 'U';
            s.environment.push_back((by)*10 + bx);
            if (leftSide) {
                square[by][bx - 2] = 'U';
                s.environment.push_back((by)*10 + bx - 1);
            }
            if (rightSide) {
                square[by][bx] = 'U';
                s.environment.push_back((by)*10 + bx + 1);
            }
        }
        break;

    case 'H':

        if (leftSide) {
            square[fy - 1][fx - 2] = 'U';
            s.environment.push_back((fy - 1)*10 + fx - 1);
            if (topSide) {
                square[fy - 2][fx - 2] = 'U';
                s.environment.push_back((fy - 2)*10 + fx - 1);
            }
            if (bottomSide) {
                square[by][fx - 2] = 'U';
                s.environment.push_back((by)*10 + fx - 1);
            }
        }

        for (size_t i = fx; i <= bx; ++i) {
            if (topSide) {
                square[fy - 2][i - 1] = 'U';
                s.environment.push_back((fy - 2)*10 + i);
            }
            if (bottomSide) {
                square[by][i - 1] = 'U';
                s.environment.push_back((by)*10 + i);
            }
        }

        if (rightSide) {
            square[by - 1][bx] = 'U';
            s.environment.push_back((by - 1)*10 + bx + 1);
            if (topSide) {
                square[by - 2][bx] = 'U';
                s.environment.push_back((by - 2)*10 + bx + 1);
            }
            if (bottomSide) {
                square[by][bx] = 'U';
                s.environment.push_back((by)*10 + bx + 1);
            }
        }
        break;

    case 'S':

        if (leftSide) {
            square[by - 1][bx - 2] = 'U';
            s.environment.push_back((by - 1)*10 + bx - 1);
        }
        if (leftSide && topSide) {
            square[by - 2][bx - 2] = 'U';
            s.environment.push_back((by - 2)*10 + bx - 1);
        }
        if (topSide) {
            square[by - 2][bx - 1] = 'U';
            s.environment.push_back((by - 2)*10 + bx);
        }
        if (topSide && rightSide) {
            square[by - 2][bx] = 'U';
            s.environment.push_back((by - 2)*10 + bx + 1);
        }
        if (rightSide) {
            square[by - 1][bx] = 'U';
            s.environment.push_back((by - 1)*10 + bx + 1);
        }
        if (rightSide && bottomSide) {
            square[by][bx] = 'U';
            s.environment.push_back((by)*10 + bx + 1);
        }
        if (bottomSide) {
            square[by][bx - 1] = 'U';
            s.environment.push_back((by)*10 + bx);
        }
        if (bottomSide && leftSide) {
            square[by][bx - 2] = 'U';
            s.environment.push_back((by)*10 + bx - 1);
        }
        break;
    }

    if (reduce) reduceAvailableCells(s);
}

/*
=============================================
    FUNCTIONS FOR USUAL INITIALIZATION
=============================================
*/

void AbstractField::addShip(AbstractShip s)
{
    ships.push_back(s);
    std::vector<coo> loc = ships.back().location;

    switch (ships.back().position) {
    case 'V':
       for (size_t i = loc.front().y, end = loc.back().y; i <= end; ++i) {
           square[i - 1][loc.front().x - 1] = 'A';
       }
        break;
    case 'H':
       for (size_t i = loc.front().x, end = loc.back().x; i <= end; ++i) {
           square[loc.front().y - 1][i - 1] = 'A';
       }
        break;
    case 'S':
        square[loc.front().y - 1][loc.front().x - 1] = 'A';
        break;
    }

   redefine(ships.back());
}

/*
=============================================
    FUNCTIONS FOR RANDOM INITIALIZATION
=============================================
*/

bool AbstractField::continueDown()
{
    if (string + extraSpace > 10) return false;
    for (size_t i = 1; i <= extraSpace; ++i) {
        if (square[string + i - 1][column - 1] != 'F') return false;
    }
    direction = 'D';
    return true;
}

bool AbstractField::continueLeft()
{
    if (column <= extraSpace) return false;
    for (size_t i = 1; i <= extraSpace; ++i) {
        if (square[string - 1][column - i - 1] != 'F') return false;
    }
    direction = 'L';
    return true;
}

bool AbstractField::continueRight()
{
    if (column + extraSpace > 10) return false;
    for (size_t i = 1; i <= extraSpace; ++i) {
        if (square[string - 1][column + i - 1] != 'F') return false;
    }
    direction = 'R';
    return true;
}

bool AbstractField::continueUp()
{
    if (string <= extraSpace) return false;
    for (size_t i = 1; i <= extraSpace; ++i) {
        if (square[string - i - 1][column - 1] != 'F') return false;
    }
    direction = 'U';
    return true;
}

bool AbstractField::defineDirection(const char potentialDirection)
{
    switch (potentialDirection)
    {
        case 'L':
        if (!continueLeft() && !continueUp() && !continueRight() && !continueDown()) return false;
        break;
        case 'U':
        if (!continueUp() && !continueRight() && !continueDown() && !continueLeft()) return false;
        break;
        case 'R':
        if (!continueRight() && !continueDown() && !continueLeft() && !continueUp()) return false;
        break;
        case 'D':
        if (!continueDown() && !continueLeft() && !continueUp() && !continueRight()) return false;
        break;
    }
    return true;
}

void AbstractField::initializeAtRandom()
{
    coo front, back;
    size_t i, n;

    for (n = 1; n < 11; ++n) {

        switch (n) {
        case 1: extraSpace = 3; break;
        case 2: case 3: extraSpace = 2; break;
        case 4: case 5: case 6: extraSpace = 1; break;
        default: extraSpace = 0; break;
        }

        core::fromStraightToCoo(availableCells[core::number(0, availableCells.size() - 1)], column, string);
        square[string - 1][column - 1] = 'A';

        if (!extraSpace) ships.push_back(AbstractShip({column, string}));
        else {

            if (!defineDirection(core::direction())) {
                square[string - 1][column - 1] = 'F';
                for (size_t i : availableCells) {
                    core::fromStraightToCoo(i, column, string);
                    square[string - 1][column - 1] = 'A';
                    if (defineDirection(core::direction())) {
                        break;
                    }
                }
            }

            switch (direction)
            {
                case 'L':
                for (i = 1; i <= extraSpace; ++i) square[string - 1][column - i - 1] = 'A';
                front = {column - extraSpace, string};
                back = {column, string};
                break;

                case 'U':
                for (i = 1; i <= extraSpace; ++i) square[string - i - 1][column - 1] = 'A';
                front = {column, string - extraSpace};
                back = {column, string};
                break;

                case 'R':
                for (i = 1; i <= extraSpace; ++i) square[string - 1][column + i - 1] = 'A';
                front = {column, string};
                back = {column + extraSpace, string};
                break;

                case 'D':
                for (i = 1; i <= extraSpace; ++i) square[string + i - 1][column - 1] = 'A';
                front = {column, string};
                back = {column, string + extraSpace};
                break;

            }
            ships.push_back(AbstractShip(front, back));
        }

        redefine(ships.back(), true);
    }

}

void AbstractField::reduceAvailableCells(AbstractShip &s)
{
    for (auto k : s.environment) {
        auto res = std::find(availableCells.begin(), availableCells.end(), k);
        if (res != availableCells.end()) availableCells.erase(res);
    }
}


}



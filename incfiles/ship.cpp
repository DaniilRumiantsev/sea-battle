#include "ship.hpp"

namespace SB {

ship::ship(QWidget *parent, size_t type, bool pre)
{
    int quantity;
    char char_type;

    switch(type) {

    case 4:
        char_type = 'F';
        quantity = 1;
        break;

    case 3:
        char_type = 'T';
        quantity = 2;
        break;

    case 2:
        char_type = 'D';
        quantity = 3;
        break;

    case 1:
        char_type = 'S';
        quantity = 4;
        break;
    }

    QVariant parentObject = QVariant();
    parentObject.setValue(this);

    this->type = type;
    itself = core::button(parent, QString("%1").arg(char_type), QString("%1").arg(quantity), QRect(0, (4 - type)*28, 115, 24));
    itself->setProperty("parentObject", parentObject);

    if (pre) {
        itself->setCheckable(true);
        itself->setAutoExclusive(true);
        if (type == 4) itself->setChecked(true);
    } else {
        itself->setEnabled(false);
    }

}

}


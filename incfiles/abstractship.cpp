#include "abstractship.hpp"

namespace SB {

AbstractShip::AbstractShip(coo placement)
    : position('S')
{
   location.push_back(placement);
   environment.push_back((placement.y - 1)*10 + placement.x);
}

AbstractShip::AbstractShip(coo front, coo back)
{
    location.push_back(front);
    location.push_back(back);

    if (location.front().x == location.back().x) {
        position = 'V';
        for (size_t y = front.y, x = front.x; y <= back.y; ++y) environment.push_back((y-1)*10 + x);
    }

    if (location.front().y == location.back().y) {
        position = 'H';
        for (size_t x = front.x, y = front.y; x <= back.x; ++x) environment.push_back((y-1)*10 + x);
    }
}

}

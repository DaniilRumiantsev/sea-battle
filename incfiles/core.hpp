#ifndef CORE_HPP
#define CORE_HPP

#include <algorithm>
#include <ctime>
#include <random>
#include <vector>

#include <QButtonGroup>
#include <QCheckBox>
#include <QFile>
#include <QLabel>
#include <QLayout>
#include <QMainWindow>
#include <QMap>
#include <QMediaPlayer>
#include <QObject>
#include <QPushButton>
#include <QRadioButton>
#include <QSlider>
#include <QTimer>

namespace SB {

class core
{

public:

    static const int all = 1;
    static const int ships = 2;
    static const int horizontal = 1;
    static const int vertical = 2;

    static QString getFromFile(QString filename);

    static QWidget* widget(QWidget *parent, QString name, QRect geometry = QRect(0, 0, 0, 0), bool show = true);
    static QLabel*  label(QWidget *parent, QString name, QString text = "", QRect geometry = QRect(0, 0, 0, 0));
    static QPushButton* button(QWidget *parent, QString name, QString text, QRect geometry = QRect(0, 0, 0, 0), bool enabled = true);
    static QCheckBox* checkBox(QWidget *parent, QString name, QString text, QRect geometry, bool enabled = true, bool checked = false);
    static QRadioButton* radioButton(QWidget *parent, QString name, QString text, QRect geometry, int value, bool enabled = true, bool checked = false);
    static QSlider* slider(QWidget* parent, QString name, QRect geometry, int min = 0, int max = 100, int value = 0);
    static QVBoxLayout *verticalLayout(QWidget* parent, QString name, QMargins margins = QMargins(0,0,0,0), size_t spacing = 0);

    static size_t number(size_t min, size_t max);
    static size_t fromCooToStraight(size_t i, size_t j);
    static void fromStraightToCoo(size_t straight, size_t &j, size_t &i);
    static char direction();

};

struct coo {
    size_t x,y;
};

}

#endif // CORE_HPP

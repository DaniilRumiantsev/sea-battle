#ifndef GAME_HPP
#define GAME_HPP

#include "incfiles/core.hpp"
#include "incfiles/prefield.hpp"
#include "incfiles/realfield.hpp"
#include "incfiles/process.hpp"

namespace SB {

class game : public QWidget
{
    Q_OBJECT

public:

    game(QWidget *parent = 0);
    ~game();

private:

/*
================================
          PROPERTIES
================================
*/

    // Settings

    bool allowMusic;
    bool allowSound;
    int musicLevel;
    int soundLevel;
    int complexityLevel;
    int language;

    // Language items

    QString langpath;
    QMap<QString, QString> phrases;

    // Vertical layouts

    QVBoxLayout *mainLayout;
    QVBoxLayout *structure;
    QVBoxLayout *mainWidgetLayout;
    QVBoxLayout *nestedLayout;

    // Main parts

    QWidget *mainWidget;
    QWidget *mainMenuWidget;
    QWidget *mainMenu;
    QWidget *helpSection;
    QWidget *aboutSection;
    QWidget *settingsSection;
    QWidget *startGame;
    QWidget *seaBattle;
    QWidget *exitPage;
    QWidget *activeGameArea;
    QWidget *activePreArea;

    QLabel *mainText;

    // Main menu buttons

    QPushButton *continueButton;
    QPushButton *startButton;
    QPushButton *settingsButton;
    QPushButton *helpButton;
    QPushButton *aboutButton;
    QPushButton *exitButton;

    // Main widget elements

    QWidget *header;
    QWidget *middle;
    QWidget *footer;
    QLabel *title;
    QPushButton *toMainMenu;

    // Settings elemennts

    QWidget *musicSection;
    QWidget *soundSection;
    QWidget *complSection;
    QWidget *langsSection;

    QLabel *musicVolume;
    QLabel *soundVolume;
    QLabel *musicVolumeValue;
    QLabel *soundVolumeValue;
    QLabel *settingsDescription;

    QCheckBox *musicButton;
    QCheckBox *soundButton;

    QSlider *musicSlider;
    QSlider *soundSlider;

    QLabel *complSettings;
    QLabel *langsSettings;

    QRadioButton *easyButton;
    QRadioButton *normButton;
    QRadioButton *hardButton;
    QRadioButton *ruButton;
    QRadioButton *enButton;

    QButtonGroup *complexityButtons;
    QButtonGroup *languagesButtons;

    // Game process properties

    process *gameProcess;

    PreField *startField;
    RealField *userField;
    RealField *pcField;

    QWidget *gameOverBox;
    QPushButton *okButton;
    QPushButton *hideButton;

    QLabel *yourField;
    QLabel *opponentField;

    QLabel *statisticsHeader;
    QLabel *statisticsShips;
    QLabel *statisticsField;
    QLabel *statisticsMoves;

    QLabel *aliveShips;
    QLabel *crashedShips;
    QLabel *yourAliveShips;
    QLabel *yourCrashedShips;
    QLabel *pcAliveShips;
    QLabel *pcCrashedShips;

    QLabel *crashedCells;
    QLabel *yourCrashedCells;
    QLabel *pcCrashedCells;

    QLabel *yourMoves;
    QLabel *pcMoves;

    QLabel *hitStatus;
    QLabel *gameOverMessage;

    // Sounds

    QMediaPlayer *backsound;
    QMediaPlayer *crashsound;
    QMediaPlayer *hitsound;

    // Exit Page properties

    QLabel *exitQuestion;
    QWidget *exitButtons;
    QPushButton *exitAccept;
    QPushButton *exitReject;

    // Another properties

    QWidget *currentSection;

/*
================================
          FUNCTIONS
================================
*/

    // Initial initializers (setters)

    void initialize();
    void setConnections();
    void setLayouts();
    void setMainWidget();
    void setMenu();
    void setMusic();
    void setSizes();
    void setSound();

    // Further initializers (setters)

    void setAbout();
    void setExitPage();
    void setGameStatistics();
    void setHelp();
    void setSeaBattle();
    void setSettings();
    void setSettingsConnections();
    void setStart();

    // Slots && changers

    void changeComplexity();
    void changeLanguage();
    void changeMusicSettings();
    void changeSoundSettings();
    void continueGame();
    void finishGame();
    void hitOccured();
    void menuOptionSelected();
    void moveFinished();
    void moveResult();
    void repeatMusic();
    void toMainMenuButtonPressed();

    // Others

    void enableCells(bool enabled = true);
    void gameOver(bool victory = true);
    void generateOpponentMove();
    void getStatistics(RealField *field);
    void loadLanguage();
    bool loadSettings();
    void playSound(QMediaPlayer *sound);
    void saveSettings();
};

}

#endif // GAME_HPP

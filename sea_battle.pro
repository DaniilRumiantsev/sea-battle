#-------------------------------------------------
#
# Project created by QtCreator 2014-11-28T21:44:55
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sea_battle
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++1y -pedantic-errors -Wall -fexec-charset=UTF-8


SOURCES += main.cpp\
        game.cpp \
    incfiles/core.cpp \
    incfiles/field.cpp \
    incfiles/abstractfield.cpp \
    incfiles/abstractship.cpp \
    incfiles/ship.cpp \
    incfiles/prefield.cpp \
    incfiles/realfield.cpp \
    incfiles/process.cpp

HEADERS  += game.hpp \
    incfiles/core.hpp \
    incfiles/field.hpp \
    incfiles/abstractfield.hpp \
    incfiles/abstractship.hpp \
    incfiles/ship.hpp \
    incfiles/prefield.hpp \
    incfiles/realfield.hpp \
    incfiles/process.hpp

OTHER_FILES += \
    style/style.css

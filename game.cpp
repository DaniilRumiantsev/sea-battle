#include "game.hpp"

namespace SB {

/*
==================================
            CONSTRUCTORS
==================================
*/

game::game(QWidget *parent)
    : QWidget(parent)
{
    setObjectName("sbGame");
    setStyleSheet(core::getFromFile("style/style.css"));

    initialize();
    setMainWidget();
    setMenu();
    setLayouts();
    setMusic();
    setSound();
    setConnections();
    setSizes();
    setWindowTitle(phrases["title"]);
    setWindowIcon(QIcon("style/images/icon.png"));
}

game::~game()
{

}

/*
==================================
        INITIAL SETTERS
==================================
*/

void game::setConnections()
{
    // Main menu buttons pressed

    connect(continueButton, &QPushButton::clicked, this, &game::continueGame);
    connect(startButton, &QPushButton::clicked, this, &game::setStart);
    connect(settingsButton, &QPushButton::clicked, this, &game::setSettings);
    connect(helpButton, &QPushButton::clicked, this, &game::setHelp);
    connect(aboutButton, &QPushButton::clicked, this, &game::setAbout);
    connect(exitButton, &QPushButton::clicked, this, &game::setExitPage);

    // to main menu button pressed

    connect(toMainMenu, &QPushButton::clicked, this, &game::toMainMenuButtonPressed);

}

void game::initialize()
{
    if (!loadSettings()) {
        allowMusic = true;
        allowSound = true;
        musicLevel = 50;
        soundLevel = 50;
        complexityLevel = 2;
        language = 'E';
    }
    loadLanguage();
}

void game::setLayouts()
{
    mainLayout = core::verticalLayout(this, "mainLayuot");
    mainLayout->addWidget(mainMenuWidget);

    structure = core::verticalLayout(mainWidget, "structure");
    structure->addWidget(header);
    structure->addWidget(middle);
    structure->addWidget(footer);
}

void game::setMainWidget()
{
    mainWidget = core::widget(this, "mainWidget", QRect(0, 0, 0, 0), false);
    header = core::widget(mainWidget, "header");
    middle = core::widget(mainWidget, "middle");
    footer = core::widget(mainWidget, "footer");
    title = core::label(header, "title", "", QRect(100, 10, 900, 60));
    toMainMenu = core::button(footer, "toMainMenu", QString("\u00AB %1").arg(phrases["mainmenu"]), QRect(50, 14, 140, 32));
}

void game::setMenu()
{
    mainMenuWidget = core::widget(this, "mainMenuWidget");
    mainMenu = core::widget(mainMenuWidget, "mainMenu", QRect(100, 300, 420, 240));
    continueButton = core::button(mainMenu, "continueButton", phrases["continue"], QRect(0, 0, 360, 32), false);
    startButton = core::button(mainMenu, "startButton", phrases["newgame"], QRect(10, 40, 360, 32));
    settingsButton = core::button(mainMenu, "settingsButton", phrases["settings"], QRect(20, 80, 360, 32));
    helpButton = core::button(mainMenu, "helpButton", phrases["help"], QRect(30, 120, 360, 32));
    aboutButton = core::button(mainMenu, "aboutButton", phrases["about"], QRect(40, 160, 360, 32));
    exitButton = core::button(mainMenu, "exitButton", phrases["exit"], QRect(50, 200, 360, 32));
}

void game::setMusic()
{
    backsound = new QMediaPlayer;
    backsound->setMedia(QUrl::fromLocalFile("sounds/background.mp3"));
    backsound->setVolume(musicLevel);
    if (allowMusic) backsound->play();
    connect(backsound, &QMediaPlayer::positionChanged, this, &game::repeatMusic);
}

void game::setSizes()
{
    showFullScreen();
    if ((width() < 1024) || (height() < 576)) {
        setGeometry(0, 0, 1024, 576);
        showNormal();
    }
}

void game::setSound()
{
    hitsound = new QMediaPlayer;
    hitsound->setMedia(QUrl::fromLocalFile("sounds/hitsound.mp3"));
    hitsound->setVolume(soundLevel);
    crashsound = new QMediaPlayer;
    crashsound->setMedia(QUrl::fromLocalFile("sounds/crashsound.mp3"));
    crashsound->setVolume(soundLevel);
}

/*
==================================
        FURTHER SETTERS
==================================
*/

void game::setAbout()
{
    menuOptionSelected();

    aboutSection = core::widget(middle, "aboutSection");
    mainText = core::label(aboutSection, "mainText", core::getFromFile(QString("languages/%1/texts/about.txt").arg(langpath)));
    currentSection = aboutSection;
    title->setText(phrases["aboutgame"]);
    mainWidgetLayout->addWidget(aboutSection);
    nestedLayout = core::verticalLayout(aboutSection, "nestedLayout", QMargins(50, 50, 50, 50));
    nestedLayout->addWidget(mainText);
}

void game::setExitPage()
{
    menuOptionSelected();

    exitPage = core::widget(middle, "exitPage");
    exitQuestion = core::label(exitPage, "exitQuestion", phrases["exitquestion"]);
    exitQuestion->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    exitButtons = core::widget(exitPage, "exitButtons");
    exitAccept = core::button(exitButtons, "exitAccept", phrases["yes"], QRect(100, 0, 100, 40));
    exitReject = core::button(exitButtons, "exitReject", phrases["no"], QRect(460, 0, 100, 40));
    currentSection = exitPage;
    title->setText(phrases["exit"]);
    mainWidgetLayout->addWidget(exitPage);
    nestedLayout = core::verticalLayout(exitPage, "nestedLayout", QMargins(50, 50, 50, 50));
    nestedLayout->addWidget(exitQuestion);
    nestedLayout->addWidget(exitButtons);
    nestedLayout->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    connect(exitReject, &QPushButton::clicked, this, &game::toMainMenuButtonPressed);
    connect(exitAccept, &QPushButton::clicked, this, &game::close);
}

void game::setGameStatistics()
{
    yourField = core::label(activeGameArea, "yourField", phrases["yourfield"], QRect(200, 15, 200, 30));
    opponentField = core::label(activeGameArea, "opponentField", phrases["opponentfield"], QRect(624, 15, 200, 30));

    statisticsHeader = core::label(activeGameArea, "statisticsHeader", phrases["statistics"], QRect(453, 60, 118, 30));
    statisticsShips = core::label(activeGameArea, "statisticsShips", phrases["ships"], QRect(453, 100, 118, 30));

    aliveShips = core::label(activeGameArea, "aliveShips", phrases["alive"], QRect(453, 130, 118, 25));
    yourAliveShips = core::label(activeGameArea, "yourAliveShips", "10", QRect(453, 155, 59, 20));
    pcAliveShips = core::label(activeGameArea, "pcAliveShips", "10", QRect(512, 155, 59, 20));

    crashedShips = core::label(activeGameArea, "crashedShips", phrases["crashed"], QRect(453, 175, 118, 25));
    yourCrashedShips = core::label(activeGameArea, "yourCrashedShips", "0", QRect(453, 200, 59, 20));
    pcCrashedShips = core::label(activeGameArea, "pcCrashedShips", "0", QRect(512, 200, 59, 20));

    statisticsField = core::label(activeGameArea, "statisticsField", phrases["field"], QRect(453, 220, 118, 30));
    crashedCells = core::label(activeGameArea, "crashedCells", phrases["crashedcells"], QRect(453, 250, 118, 25));
    yourCrashedCells = core::label(activeGameArea, "yourCrashedCells", "0%", QRect(453, 275, 59, 20));
    pcCrashedCells = core::label(activeGameArea, "pcCrashedCells", "0%", QRect(512, 275, 59, 20));

    statisticsMoves = core::label(activeGameArea, "statisticsMoves", phrases["moves"], QRect(453, 295, 120, 30));
    yourMoves = core::label(activeGameArea, "yourMoves", "0", QRect(453, 330, 59, 20));
    pcMoves = core::label(activeGameArea, "pcMoves", "0", QRect(512, 330, 59, 20));

    hitStatus = core::label(activeGameArea, "hitStatus", phrases["wait"], QRect(212, 390, 600, 40));

    yourField->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    opponentField->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    statisticsHeader->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    statisticsShips->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    statisticsField->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    aliveShips->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    crashedShips->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    yourAliveShips->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    yourCrashedShips->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    pcAliveShips->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    pcCrashedShips->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    crashedCells->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    yourCrashedCells->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    pcCrashedCells->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    statisticsMoves->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    yourMoves->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    pcMoves->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    hitStatus->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
}

void game::setHelp()
{
    menuOptionSelected();

    helpSection = core::widget(middle, "helpSection");
    mainText = core::label(helpSection, "mainText", core::getFromFile(QString("languages/%1/texts/help.txt").arg(langpath)));
    currentSection = helpSection;
    title->setText(phrases["help"]);
    mainWidgetLayout->addWidget(helpSection);
    nestedLayout = core::verticalLayout(helpSection, "nestedLayout");
    nestedLayout->addWidget(mainText);
}

void game::setSeaBattle()
{
    seaBattle = core::widget(middle, "seaBattle");
    currentSection = seaBattle;
    title->setText(phrases["newbattle"]);
    continueButton->setEnabled(true);
    activeGameArea = core::widget(seaBattle, "activeGameArea");

    setGameStatistics();

    userField = new RealField(activeGameArea, &phrases, langpath, "userField", QRect(133, 60, 320, 320), startField->abstraction, QRect(9, 60, 115, 120));
    pcField = new RealField(activeGameArea, &phrases, langpath, "pcField", QRect(571, 60, 320, 320), new AbstractField(true), QRect(900, 60, 115, 120));

    delete startGame;

    userField->abstraction->optimize();
    pcField->abstraction->optimize();
    userField->abstraction->allowAllCells();
    pcField->abstraction->allowAllCells();
    userField->reflectAbstraction(core::all);

    gameProcess = new process(complexityLevel);

    for (size_t i = 0; i < 10; ++i) {
        for (size_t j = 0; j < 10; ++j) {
            connect(pcField->cells[i][j], &QPushButton::clicked, this, &game::hitOccured);
            connect(userField->cells[i][j], &QPushButton::clicked, this, &game::hitOccured);
        }
    }

    mainWidgetLayout->addWidget(seaBattle);
    nestedLayout = core::verticalLayout(seaBattle, "nestedLayout");
    nestedLayout->addWidget(activeGameArea);
    nestedLayout->setAlignment(activeGameArea, Qt::AlignHCenter | Qt::AlignVCenter);

}

void game::setSettings()
{
    menuOptionSelected();

    settingsSection = core::widget(middle, "settingsSection");
    currentSection = settingsSection;
    title->setText(phrases["settings"]);

    musicSection = core::widget(settingsSection, "musicSection");
    soundSection = core::widget(settingsSection, "soundSection");
    complSection = core::widget(settingsSection, "complSection");
    langsSection = core::widget(settingsSection, "langsSection");

    musicButton = core::checkBox(musicSection, "musicButton", phrases["enablemusic"], QRect(0, 0, 200, 20), true, allowMusic);
    musicVolume = core::label(musicSection, "musicVolume", QString("%1: ").arg(phrases["volume"]), QRect(0, 25, 140, 40));
    musicSlider = core::slider(musicSection, "musicSlider", QRect(150, 30, 300, 30), 0, 100, musicLevel);
    musicVolumeValue = core::label(musicSection, "musicVolumeValue", QString("%1").arg(musicSlider->value()), QRect(470, 25, 100, 40));

    soundButton = core::checkBox(soundSection, "soundButton", phrases["enablesound"], QRect(0, 0, 200, 25), true, allowSound);
    soundVolume = core::label(soundSection, "soundVolume", QString("%1: ").arg(phrases["volume"]), QRect(0, 25, 140, 40));
    soundSlider = core::slider(soundSection, "soundSlider", QRect(150, 30, 300, 30), 0, 100, soundLevel);
    soundVolumeValue = core::label(soundSection, "soundVolumeValue", QString("%1").arg(soundSlider->value()), QRect(470, 25, 100, 40));

    complSettings = core::label(complSection, "complSettings", phrases["complexity"], QRect(0, 0, 600, 40));
    easyButton = core::radioButton(complSection, "easyButton", phrases["easy"], QRect(40, 40, 200, 25), 1, true, complexityLevel == 1);
    normButton = core::radioButton(complSection, "normButton", phrases["medium"], QRect(40, 65, 200, 25), 2, true, complexityLevel == 2);
    hardButton = core::radioButton(complSection, "hardButton", phrases["hard"], QRect(40, 90, 200, 25), 3, true, complexityLevel == 3);

    complexityButtons = new QButtonGroup(settingsSection);
    complexityButtons->addButton(easyButton);
    complexityButtons->addButton(normButton);
    complexityButtons->addButton(hardButton);

    langsSettings = core::label(langsSection, "langsSettings", phrases["language"], QRect(0, 0, 600, 40));
    enButton = core::radioButton(langsSection, "enButton", "English", QRect(40, 40, 200, 25), 'E', true, language == 'E');
    ruButton = core::radioButton(langsSection, "ruButton", "Русский", QRect(40, 65, 200, 25), 'R', true, language == 'R');

    languagesButtons = new QButtonGroup(settingsSection);
    languagesButtons->addButton(enButton);
    languagesButtons->addButton(ruButton);

    settingsDescription = core::label(settingsSection, "settingsDescription", core::getFromFile(QString("languages/%1/texts/settings.txt").arg(langpath)), QRect(724, 0, 300, 300));

    mainWidgetLayout->addWidget(settingsSection);
    nestedLayout = core::verticalLayout(settingsSection, "nestedLayout", QMargins(30, 10, 30, 0), 5);
    nestedLayout->addWidget(musicSection);
    nestedLayout->addWidget(soundSection);
    nestedLayout->addWidget(complSection);
    nestedLayout->addWidget(langsSection);
    nestedLayout->setStretch(0, 2);
    nestedLayout->setStretch(1, 2);
    nestedLayout->setStretch(2, 3);
    nestedLayout->setStretch(3, 2);

    setSettingsConnections();
}

void game::setSettingsConnections()
{
    connect(musicButton, &QRadioButton::clicked, this, &game::changeMusicSettings);
    connect(musicSlider, &QSlider::valueChanged, this, &game::changeMusicSettings);
    connect(soundButton, &QRadioButton::clicked, this, &game::changeSoundSettings);
    connect(soundSlider, &QSlider::valueChanged, this, &game::changeSoundSettings);
    connect(easyButton, &QRadioButton::clicked, this, &game::changeComplexity);
    connect(normButton, &QRadioButton::clicked, this, &game::changeComplexity);
    connect(hardButton, &QRadioButton::clicked, this, &game::changeComplexity);
    connect(enButton, &QRadioButton::clicked, this, &game::changeLanguage);
    connect(ruButton, &QRadioButton::clicked, this, &game::changeLanguage);
}

void game::setStart()
{
    menuOptionSelected();

    if (currentSection == seaBattle) {
        delete currentSection;
        delete gameProcess;
    }
    continueButton->setEnabled(false);
    startGame = core::widget(middle, "startGame");
    currentSection = startGame;
    title->setText(phrases["shipplacement"]);

    activePreArea = core::widget(startGame, "activePreArea");
    startField = new PreField(activePreArea, &phrases, langpath);
    mainWidgetLayout->addWidget(startGame);
    nestedLayout = core::verticalLayout(startGame, "nestedLayout");
    nestedLayout->addWidget(activePreArea);
    nestedLayout->setAlignment(activePreArea, Qt::AlignHCenter | Qt::AlignVCenter);

    connect(startField->acceptButton, &QPushButton::clicked, this, &game::setSeaBattle);

}

/*
==================================
            SLOTS
==================================
*/

void game::changeComplexity()
{
    complexityLevel = sender()->property("value").value<int>();
}

void game::changeLanguage()
{
    language = sender()->property("value").value<int>();
}

void game::changeMusicSettings()
{
    if (musicButton->isChecked()) {
        allowMusic = true;
        musicLevel = musicSlider->value();
        musicVolumeValue->setText(QString("%1").arg(musicLevel));
        backsound->setVolume(musicLevel);
        backsound->play();
        musicSlider->setEnabled(true);
    } else {
        allowMusic = false;
        backsound->stop();
        musicSlider->setEnabled(false);
    }
}

void game::changeSoundSettings()
{
    if (soundButton->isChecked()) {
        allowSound = true;
        soundLevel = soundSlider->value();
        soundVolumeValue->setText(QString("%1").arg(soundLevel));
        crashsound->setVolume(soundLevel);
        hitsound->setVolume(soundLevel);
        soundSlider->setEnabled(true);
    } else {
        allowSound = false;
        soundSlider->setEnabled(false);
    }
}

void game::continueGame()
{
    menuOptionSelected();

    currentSection = seaBattle;
    currentSection->show();
    title->setText(phrases["continuebattle"]);
    mainWidgetLayout->addWidget(seaBattle);
    nestedLayout = core::verticalLayout(seaBattle, "nestedLayout");
    nestedLayout->addWidget(activeGameArea);
    nestedLayout->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
}

void game::hitOccured()
{
    size_t x = sender()->property("column").value<size_t>();
    size_t y = sender()->property("string").value<size_t>();
    RealField* targetField = static_cast<RealField*>(sender()->property("field").value<field*>());
    gameProcess->success = targetField->eraseCell(y - 1, x - 1);

    if (!gameProcess->move && gameProcess->cellsToHit.size()) {
        if (gameProcess->success) gameProcess->cellsToHit.clear();
        else {
            auto res = std::find(gameProcess->cellsToHit.begin(), gameProcess->cellsToHit.end(), core::fromCooToStraight(y - 1, x - 1));
            if (res != gameProcess->cellsToHit.end()) gameProcess->cellsToHit.erase(res);
        }
    }

    if (gameProcess->success) {
        std::vector<size_t>* argument = nullptr;
        if (!gameProcess->move && !gameProcess->cellsToHit.size()) argument = &gameProcess->cellsToHit;
        targetField->alterField(coo{x,y}, argument, gameProcess->complexity);
    }

    ++(gameProcess->move ? gameProcess->userMovesDone : gameProcess->pcMovesDone);

    getStatistics(targetField);
    playSound(gameProcess->success ? crashsound : hitsound) ;
    enableCells(false);

    hitStatus->setText("");

    gameProcess->moveFinTimer = new QTimer(this);
    connect(gameProcess->moveFinTimer, &QTimer::timeout, this, &game::moveFinished);
    gameProcess->moveFinTimer->start(targetField == pcField ? 500 : 800);

    gameProcess->moveResTimer = new QTimer(this);
    connect(gameProcess->moveResTimer, &QTimer::timeout, this, &game::moveResult);
    gameProcess->moveResTimer->start(targetField == pcField ? 150 : 300);
}

void game::menuOptionSelected()
{
    mainLayout->removeWidget(mainMenuWidget);
    mainLayout->addWidget(mainWidget);
    mainMenuWidget->hide();
    mainWidget->show();
    mainWidgetLayout = core::verticalLayout(middle, "mainWidgetLayout");
}

void game::moveFinished()
{
    delete gameProcess->moveFinTimer;

    if (!userField->aliveShips || !pcField->aliveShips) {
        gameOver(gameProcess->move ? true : false);
        return;
    }

    QString who;

    if (gameProcess->move) {

        if (gameProcess->success) {
            who = phrases["you"];
            enableCells(true);
        }
        else {
            who = phrases["opponent"];
            gameProcess->move = false;
        }

    } else {

        if (gameProcess->success) who = phrases["opponent"];
        else {
            who = phrases["you"];
            gameProcess->move = true;
            enableCells(true);
        }
    }

    title->setText(QString("%1: %2").arg(phrases["newact"], who));
    hitStatus->setText(phrases["wait"]);
    hitStatus->setStyleSheet("color: #fff;");

    if (!gameProcess->move) generateOpponentMove();

}

void game::moveResult()
{
    delete gameProcess->moveResTimer;
    if (gameProcess->success) {
        hitStatus->setText(phrases["successful"]);
        hitStatus->setStyleSheet("color: #4b4;");
    } else {
        hitStatus->setText(phrases["bloomer"]);
        hitStatus->setStyleSheet("color: #b44;");
    }
}

void game::repeatMusic()
{
    if (allowMusic) backsound->play();
}

void game::toMainMenuButtonPressed()
{
    delete mainWidgetLayout;
    delete nestedLayout;
    mainLayout->removeWidget(mainWidget);
    mainLayout->addWidget(mainMenuWidget);
    mainWidget->hide();
    mainMenuWidget->show();
    if (currentSection == settingsSection) saveSettings();
    if ((currentSection == seaBattle)) {
        currentSection->hide();
    } else delete currentSection;
}

/*
==================================
            OTHERS
==================================
*/

void game::enableCells(bool enabled)
{
    for (auto i : pcField->abstraction->availableCells) {
            size_t column, string;
            core::fromStraightToCoo(i, column, string);
            pcField->cells[string - 1][column - 1]->setEnabled(enabled);
    }
}

void game::gameOver(bool victory)
{
    continueButton->setEnabled(false);
    QString text = QString("%1!\n%2 %3!").arg(phrases["gameover"]).arg(phrases["you"]).arg(victory ? phrases["won"] : phrases["lost"]);
    pcField->reflectAbstraction(core::all);
    gameOverBox = core::widget(activeGameArea, "gameOver", QRect(312, 100, 400, 236));
    gameOverMessage = core::label(gameOverBox, "gameOverMessage", text, QRect(0, 0, 400, 180));
    gameOverMessage->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    okButton = core::button(gameOverBox, "okButton", phrases["accept"], QRect(140, 190, 120, 32));
    hideButton = core::button(gameOverBox, "hideButton", "", QRect(365, 15, 20, 20));
    connect(okButton, &QPushButton::clicked, this, &game::toMainMenuButtonPressed);
    connect(hideButton, &QPushButton::clicked, gameOverBox, &QWidget::close);
}

void game::generateOpponentMove()
{
    size_t cellToHit, column, string;
    if (gameProcess->cellsToHit.size()) cellToHit = gameProcess->cellsToHit[core::number(0, gameProcess->cellsToHit.size() - 1)];
    else cellToHit = userField->abstraction->availableCells[core::number(0, userField->abstraction->availableCells.size() - 1)];
    core::fromStraightToCoo(cellToHit, column, string);

    userField->cells[string - 1][column - 1]->setEnabled(true);
    userField->cells[string - 1][column - 1]->click();
    userField->cells[string - 1][column - 1]->setEnabled(false);
}

void game::getStatistics(RealField *field)
{
    if (field == userField) {
        yourCrashedCells->setText(QString("%1%").arg(100 - userField->abstraction->availableCells.size()));
        yourAliveShips->setText(QString("%1").arg(userField->aliveShips));
        yourCrashedShips->setText(QString("%1").arg(10 - userField->aliveShips));
        pcMoves->setText(QString("%1").arg(gameProcess->pcMovesDone));
        return;
    }

    if (field == pcField) {
        pcCrashedCells->setText(QString("%1%").arg(100 - pcField->abstraction->availableCells.size()));
        pcAliveShips->setText(QString("%1").arg(pcField->aliveShips));
        pcCrashedShips->setText(QString("%1").arg(10 - pcField->aliveShips));
        yourMoves->setText(QString("%1").arg(gameProcess->userMovesDone));
        return;
    }
}

void game::loadLanguage()
{
    langpath = language == 'R' ? "ru" : "en";
    QFile lng(QString("languages/%1/general.lng").arg(langpath));
    lng.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream load(&lng);

    while (!load.atEnd()) {
        QString pair = load.readLine();
        int i = 0, end = pair.length();
        QString key, value;
        while ((i < end) && (pair[i] != QChar(' '))) key.push_back(pair[i++]);
        while ((i < end) && (pair[i++] != QChar('='))); // Miss
        while ((i < end) && (pair[i++] != QChar('"'))); // Miss
        while ((i < end) && (pair[i] != QChar('"'))) value.push_back(pair[i++]);
        phrases.insert(key, value);
    }
}

bool game::loadSettings()
{
    QFile file("settings.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return false;

    size_t i = 0;
    QTextStream load(&file);
    while ((++i < 7) && !load.atEnd()) {
        int option = load.readLine().toInt();
        switch (i) {
        case 1: allowMusic = option; break;
        case 2: allowSound = option; break;
        case 3: musicLevel = ((option >= 0 && option <= 100) ? option : 0); break;
        case 4: soundLevel = ((option >= 0 && option <= 100) ? option : 0); break;
        case 5: complexityLevel = ((option >= 1 && option <= 3) ? option : 0); break;
        case 6: language = ((option == 'E' || option == 'R') ? option : 'E'); break;
        }
    }

    return (i == 7) ? true : false;
}

void game::playSound(QMediaPlayer *sound)
{
    if (allowSound) sound->play();
}

void game::saveSettings()
{
    QFile file("settings.txt");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) return;

    QTextStream save(&file);
    save << allowMusic << "\n";
    save << allowSound << "\n";
    save << musicLevel << "\n";
    save << soundLevel << "\n";
    save << complexityLevel << "\n";
    save << language << "\n";
}

}

